#!/bin/bash

echo "$(tput bold)START OSyRIS:$(tput sgr0)"

{
    gunicorn app.main:app --workers 4 --worker-class uvicorn.workers.UvicornWorker --bind 0.0.0.0:4321
} || {
    uvicorn app.main:app
}