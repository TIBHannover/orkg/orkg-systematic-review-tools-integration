FROM python:3.10 as requirements-stage

WORKDIR /tmp

RUN pip install poetry

COPY ./pyproject.toml ./poetry.lock* /tmp/

RUN poetry export -f requirements.txt --output requirements.txt --without-hashes

FROM python:3.10

WORKDIR /orkg-osyris

COPY --from=requirements-stage /tmp/requirements.txt /orkg-osyris/requirements.txt

RUN pip install --upgrade pip
RUN pip install --no-cache-dir --upgrade -r /orkg-osyris/requirements.txt

COPY ./app /orkg-osyris/app

CMD ["gunicorn", "app.main:app", "--workers", "4",  "--timeout", "0", "--worker-class", "uvicorn.workers.UvicornWorker", "--bind", "0.0.0.0:4321", "--access-logfile=-", "--error-logfile=-"]