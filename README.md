# ORKG OSyRIS API

**OSyRIS** stands for **ORKG Systematic Review Integration Service**.

Systematic reviews represent a popular research methodology, most prominent among
healthcare researchers. Systematic reviews aim to compile a state-of-the-art for a
specific research question. At that, they compile key contributions from relevant
papers in a comparative fashion. Having said this, synthesised data from systematic
reviews fulfill a purpose similar to **ORKG Comparisons**. **OSyRIS** is a
microservice that handles seamless, semi-automatic integration of systematic review
synthesis data from arbitrary systematic review services (such as
[CADIMA](https://www.cadima.info),
[Covidence](https://www.covidence.org/) or
[Rayyan](https://www.rayyan.ai/)). Known from numerous web applications, the integration
provides a user interface via an interactive widget (like *Google Maps* or *X Posts*)
that can be integrated by review service providers in a matter of minutes.  
  
This is the source code repository for the service associated with the thesis
*“Integration of Systematic Review Services with a Scholarly Knowledge Graph”* (2023)
in fulfillment of the requirements for the degree of Master of Science in Computer
Science by  
  
Thassilo Martin Schiepanski

> The thesis related with this project is located at
> [https://repo.uni-hannover.de](https://www.repo.uni-hannover.de/handle/123456789/15287) (DOI: *10.15488/15168*).  
> The source code for the experiments related with this project are located at
> [https://data.uni-hannover.de](https://doi.org/10.25835/wexlp2ye) (DOI: *10.25835/wexlp2ye*).

## Prerequisites

The service requires a Python environment, versioned `3.10` or above.

## Architecture

The service is injecting a modal widget into a designated systematic review service's
web application document. The widget comprises a button to export review data upon
demand. On button click, the widget opens a series of modals to complete the export
process. Every systematic review synthesis maintains an equivalent tabular data
structure. The table column names (or row names, if inverted) are therefore
representative for a specific reviewed aspect. In the knowledge graph, the aspects would
therefore represent predicates. The export process thus includes a mandatory, interactive
step that fosters internal concept homogeneity of **ORKG**; namely, computing existing
predicates in the graph that supposedly encode the same concept as described with the
review data.  
  
The service maintains a server application which serves the REST-API, but also the widget
asset files. The service can be integrated into an arbitrary web-based systematic review
service application by a simple script reference. Upon use, the widget handles
communication with the service's REST-API, which itself hides communication with the
underlying **ORKG** backend.  
  
To comply with the **ORKG** technology stack and model, the project adopts the
technological structure and dependencies of existing *ORKG* microservices (a.o.
**SimComp API**, and **NLP API**). More specifically, the service builds on the **FastAPI**
framework in a **Python** runtime environment. The widget UI uses styles from the **ORKG**
frontend application as far as accoring rules existed.

> To preserve the state of the academic work and foster reproducibility, applicable
> styles of the **ORKG** frontend application were copied into a standalone file. Yet,
> the frontend hosted stylesheet can interchangeably be reused with a typical stylesheet
> reference in the widget markup.

## Installation

``` commandline
git clone https://https://gitlab.com/TIBHannover/orkg/orkg-systematic-review-tools-integration.git

cd orkg-systematic-review-tools-integration
```

## Deployment

### With ``docker-compose``

> :warning: The Docker setup has not yet been tested in runtime. In oder to integrate the
> service into the **ORKG** ecosystem, the provided configuration might be required to be
> reviewed and possibly asjusted.

``` commandline
docker-compose up -d
```

### Manually

The service can be run using the Bash script `run#start.sh` provided in the project root
directory. `run#debug.sh` spins up a debugging environment, including hot module reloading.

``` commandline
./run#start.sh || ./run#debug.sh
```

> The debugging environment opens a user experience sandbox in a web browser. In case the
> sandbox page does not open automatically, simply load the file at `./tests/sandbox/sandbox.en.html`
> in a web browser by hand.

## API Documentation

The API is built upon the **FastAPI** framework. Inherently, a full specification of API
endoints is automatically compiled durin application build by [OpenAPI](openapi.json).

## Systematic Review Service Integration

Since **OSyRIS** is not targeted towards a specific systematic review service, but allows
for an arbitrary application to integrate it, the following integration requisites are set
to be adhered to. In its purest form, the integration concludes to just two lines of code.

> Systematic review services are autonomous services and hence come with their own
> development departments. This integration guide is therefore written for developers of
> systematic review service tools.

### 1. Script

In the head of the web document of choice – i.e. the markup of the page at which the export
shall be available – place the widget script tag.

``` html
<head>
    <!-- WIDGET SCRIPT TAG -->
    <script defer src="https://orkg.org/widget/widget.js"></script>
    <!-- - -->
</head>
```

#### i18n / Language

The widget is available in different languages, which can be set using the respective
2-letter ISO language code to the query parameter `lang`, e.g. `https://orkg.org/widget/widget.js?lang=de`.  
  
Available languages (code): `English (en)`, `German (de)`

> :speech_balloon: Feel free to contact **ORKG** to ask for a new language to be added.

#### Sandbox Mode

To test the widget integration in a safe environment, simply use the unary query parameter
`dev` (`https://orkg.org/widget/widget.js?lang=de`). The sandbox mode works with the host
`https://sandbox.orkg.org` instead of the productive system.

### 2. Export Button

Technically, the export button could automatically be injected at any point in the DOM,
such as to floating at a fixed position over the content. To enhance seamless integration
from a stylistic perspective, the button can be placed in a position of choice, just like
an ordinary button element. Consider the example below:

``` html
    <div class="share">
        <button type="button" onclick="SHARE.mail()">Send Mail</button>
        <button type="button" onclick="SHARE.toCSV()">Export to CSV</button>
        <!-- WIDGET EXPORT BUTTON TAG -->
        <orkg-export></orkg-export>
        <!-- - -->
    </div>
<body>
```

In fact, the export button is a qualified, registered HTML element. It can be styled with
CSS to fit in with the specific layout.

> The default styling of the button is set to depict **ORKG** as an external, autonomous
> platform. Also, it is aligned with the corporate design guidelines and aims to maintain
> a (broadly) uniform appearance across different hosting review services. That being said,
> custom styling should merely change minor layout-specific aspects. In case of  doubt,
> feel free to contact **ORKG**.

#### Attribute Modifiers

Besides existing attributes on the `HTMLElement` class, the button is responsive with the
following custom attributes:

| Attribute Name | Purpose Description | Argument Description |
| :------------- | :------------------ | :------------------- |
| `orkg-data-callback` | Define a callback which to invoke for reading relevant systematic review data. | Identifier of callback bound to window scope. |
| `orkg-small` | Display a smaller button by default.  | `N/A` |

### 3. Read Callback Definition

A comprehensive review has shown that different systematic review tools come with
different approaches to how data is presented and manipulated. Therefore, each
application handles a specific data structure, internally. **OSyRIS** requires a
function that reads the systematic review data in a standard format as to be invoked
once the export was triggered. The callback must be given to the respective attribute
on the element (see above). The callback must return the data as a JSON object
sufficing the followinf interface:

``` typescript
interface ITruth {
  [index: number]: {
    authorNames: string|string[];
    paperName: string;
    strongTruth: {
      [pivot_label: string]: string|number|boolean;
    }[];

    paperDoi?: string;
    paperYear?: number;
  }
}
```

| Property | Description |
| :------- | :---------- |
| `authorNames` | List of names of paper authors |
| `paperTitle` | Title of paper |
| `strongTruth` | Predicate-object tuples in key-value association. In other words, these are the data extraction aspects and the specific associated data. |

**Example:**

``` html
<!DOCTYPE html>
<html>
    <head>
        <script>
            function readDataForORKG() {
                // ...

                return [
                  {
                    "authorNames": [
                      "Example Author 1", "Example Author 2"
                    ],
                    "paperTitle": "Example Paper",
                    "strongTruth": {
                      "Extraction methods": "manual",
                      "Worst complexity": "O(log n)",
                      "F1 score": 0.9
                    }
                  }
                ];
            }
        </script>
    </head>
    <body>
        <orkg-export orkg-data-callback="readDataForORKG"></orkg-export>
    </body>
</html>
```

The export element class `ORKGExportElement` provides a static helper
function to transpile CSV data strings to a `ITruth` compliant model.

``` javascript
const model = ORKGExportElement
    .parseCSVString(CSV_DATA, mappings = {
        authorNames: "paper:authors", // Specify column referring to the author names 
        paperName: "paper:title",     // Specify column referring to the paper title
        paperDoi: "paper:doi",        // Specify column referring to the paper DOI (optional)
        paperYear "paper:year"        // Specify column referring to the paper publication year (optional)
    }, [],                            // Specify columns to ignore
    ",", ";");                        // (1) Delimiter (comma by default)
                                      // (2) Nested delimiter (semicolon by default; used to split multiple author names)
```

**Example**

### 4. Events `optional`

To allow for an even more refined data export flow, the export button
element emits certain events that can be listened for.

| Event Name | Event Detail | Event Cause | exemplary Usage |
| :--------- | :----------- | :---------- | :-------------- |
| `error` | Error message | The SR export process aborted with an error. | Handle the error message with according UI feedback. |
| `export` | URL of Comparison resulted in ORKG  | The SR export process was successful. | Hide the export button in order not to allow for immediate override exports. |

**Examples:**

<sub><b>A. JS (vanilla)</b></sub>
``` javascript
const exportElement = document.querySelector("orkg-export");
exportElement.addEventListener("export", e => {
  document.querySelector("p#export-info")
  .textContent = `The data is now available in ORKG at '${e.details}'.`;
});
```

<sub><b>B. React JS</b></sub>
``` jsx
class App extends React.Component {
  constructor(props) {
    super(props);
    
    this.state = {
      shareInfo: null
    };
  }
  
  handleClick(e) {
    this.setState({
      shareInfo: `The data is now available in ORKG at '${e.details}'.`
    });
  }
  
  return (
    <p class="info">{this.state.shareInfo}</p>
    <button type="button" onclick="SHARE.mail()">Send Mail</button>
    <button type="button" onclick="SHARE.toCSV()">Export to CSV</button>
    <orkg-export onClick={handleClick}></orkg-export>
  );
}
```

<sub><b>C. Inline HTML</b></sub>
``` html
<orkg-export orkg-data-callback="readDataForORKG" onerror="tableWarn()"></orkg-export>
```

<sub>2023 (c) Thassilo Martin Schiepanski | Open Research Knowledge Graph Project and Contributors</sub>