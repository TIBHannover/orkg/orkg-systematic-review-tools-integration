import os
from os.path import isfile, join
import re
import json


i18n_path = join(os.path.dirname(__file__), "../app/static/i18n")
public_path = join(os.path.dirname(__file__), "../app/static/public")


print("\x1b[1mBUILD WIDGET (i18n)\x1b[0m")

with open(join(os.path.dirname(__file__), "../app/static/widget.raw.html")) as raw_widget_markup_file:
    raw_widget_markup = "".join(raw_widget_markup_file.readlines())

    for translation_markup_name in os.listdir(i18n_path):
        translation_markup_path = join(i18n_path, translation_markup_name)

        if not (isfile(translation_markup_path) or re.compile(r"^[a-z]{2}\.json$", "i").match(translation_markup_name)):
            continue
        
        with open(translation_markup_path) as translation_json:
            translation = json.load(translation_json)

            with open(join(public_path, "widget.{}".format(re.sub(r"\.json$", ".html", translation_markup_name))), "w") as built_widget_markup_file:
                marks = re.findall(r"{{ [a-zA-Z_]+ }}", raw_widget_markup)
                marks = list(set(marks))
                
                built_widget_markup = raw_widget_markup
                for mark in marks:
                    mark_id = mark[2:-2].strip()
                    substitute = translation[mark_id] if (mark_id in translation) else ""
                    built_widget_markup = re.sub(mark, substitute, built_widget_markup)
                
                built_widget_markup_file.write(built_widget_markup)