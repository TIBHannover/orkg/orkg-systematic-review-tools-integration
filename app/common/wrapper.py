import json
import datetime
import uuid


class ResponseWrapper:
    @staticmethod
    def write_json(data, input_path):
        with open(input_path, "w") as f:
            json.dump(data, f, indent = 4)
      
    @staticmethod
    def wrap_json(json_response):
        base_response = {
            "timestamp": datetime.datetime.now(),
            "uuid": uuid.uuid4(),
            "payload": json_response
        }

        return base_response