import http.client
import json
from urllib.parse import urlencode, quote
from fastapi import HTTPException


_config = {
    "host_prod": "orkg.org",
    "host_dev": "sandbox.orkg.org"
}


def _request(method: str, pathname: str, query: dict[str, str], headers: dict[str, str], body: any = None, is_dev_mode: bool = False) -> dict:	
    endpoint: str = "{}{}".format(pathname, "?{}".format(urlencode(query)) if query else "")
    
    if is_dev_mode:
        print("\x1b[1mREQUEST DATA:\x1b[0m\n{}".format(body if body else "None"))

    body = json.dumps(body) if body != None else body
    
    conn = http.client.HTTPSConnection(_config["host_dev"] if is_dev_mode else _config["host_prod"])
    conn.request(method, endpoint, body, headers)
    
    response = conn.getresponse()
    
    raw_data = response.read().decode()
    data = None
    try:
        data = json.loads(raw_data)
    except:
        data = raw_data
    
    if(str(response.status)[0] != "2"):
        detail = data["message"] if "message" in data else data
        ex = HTTPException(status_code = response.status, detail = detail)
        print("\x1b[1m\x1b[31mREQUEST ERROR:\x1b[0m\n{}".format("{}: {}".format(response.status, detail)))
        raise ex

    if is_dev_mode:
        print("\x1b[1mRESPONSE DATA:\x1b[0m\n{}".format(data if data else "None"))

    return {
        "headers": dict(response.getheaders()),
        "data": data
    }


def get(pathname: str = "/", query: str = None, headers: dict[str, str] = {}, is_dev_mode: bool = False) -> dict:
    return _request("GET", pathname, query, headers, None, is_dev_mode)

def post(body: any, pathname: str = "/", query: str = None, headers: dict[str, str] = {}, is_dev_mode: bool = False) -> dict:
    return _request("POST", pathname, query, headers, body, is_dev_mode)

def sparql(query, is_dev_mode: bool = False):
    return _request("GET", "/triplestore", {
        "query": quote(query.strip())
    }, {
        "Accept": "application/sparql-results+json"
    }, None, is_dev_mode)["results"]["bindings"]