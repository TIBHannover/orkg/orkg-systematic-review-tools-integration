import http

from typing import Optional
from fastapi import APIRouter, Query, Header

from app.common.decorators import log
from app.common.wrapper import ResponseWrapper
from app.models.submission import SubmissionRequest, SubmissionResponse
from app.services.submission import service


router = APIRouter(tags = [ "submit" ])


@router.post(
    "/submit",
    response_model = SubmissionResponse,
    status_code = http.HTTPStatus.OK,
)
@log(__name__)
def submit(req: SubmissionRequest, dev: str = Query(default = None), authorization: Optional[str] = Header(None)):
    is_dev_mode = (dev != None)
    
    return ResponseWrapper.wrap_json(service.submit(req.payload, is_dev_mode, authorization))