import http

from fastapi import APIRouter, Query

from app.common.decorators import log
from app.common.wrapper import ResponseWrapper
from app.models.recommendation import RecommendationRequest, RecommendationResponse
from app.services.recommendation import service


router = APIRouter(tags = [ "recommendation" ])


@router.post(
    "/recommend",
    response_model = RecommendationResponse,
    status_code = http.HTTPStatus.OK,
)
@log(__name__)
def recommend(req: RecommendationRequest, dev: str = Query(default = None)):
    is_dev_mode = (dev != None)
    
    return ResponseWrapper.wrap_json(service.recommend(req.payload))