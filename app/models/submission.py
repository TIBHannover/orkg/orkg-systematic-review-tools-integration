from typing_extensions import TypedDict, NotRequired
from pydantic import BaseModel

from app.models.response import Response


class SubmissionRequestDataRecord(TypedDict):
    paperName: str
    authorNames: list[str]
    strongTruth: dict[str, str|int|float|bool|None]
    
    paperDoi: NotRequired[str]
    paperYear: NotRequired[int]

class SubmissionRequestMappingRecord(TypedDict):
    label: str
    resourceId: str
    resourceType: str

class SubmissionRequestData(TypedDict):
    data: list[SubmissionRequestDataRecord]
    mappings: dict[str, SubmissionRequestMappingRecord]


class SubmissionRequest(BaseModel):
    payload: SubmissionRequestData = {}

class SubmissionResponse(Response):
    payload: list[str]