from datetime import datetime
from typing import Any
from uuid import UUID

from pydantic import BaseModel


class Response(BaseModel):
    timestamp: datetime
    uuid: UUID
    payload: Any