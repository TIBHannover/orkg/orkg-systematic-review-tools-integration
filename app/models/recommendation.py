from typing_extensions import TypedDict, NotRequired
from pydantic import BaseModel

from app.models.response import Response


class RecommendationRequestData(TypedDict):
    k: int
    truth: dict[str, list[str|int|float|bool|None]]


class RecommendationResponseDataRecordDetails(TypedDict):
    resourceId: str
    resourceType: str
    
    description: NotRequired[str]
    link: NotRequired[str]

class RecommendationResponseDataRecord(TypedDict):
    label: str
    score: float
    details: RecommendationResponseDataRecordDetails

RecommendationResponseData = dict[str, list[RecommendationResponseDataRecord]]


class RecommendationRequest(BaseModel):
    payload: RecommendationRequestData = {}

class RecommendationResponse(Response):
    payload: RecommendationResponseData