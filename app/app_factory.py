import os

from fastapi import FastAPI, status
from fastapi.encoders import jsonable_encoder
from fastapi.exceptions import RequestValidationError
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import JSONResponse
from fastapi.staticfiles import StaticFiles

from app.common.errors import OrkgOsyrisApiError
from app.common.wrapper import ResponseWrapper
from app.routers import submission
from app.routers import recommendation

from app.__version import __version__


def create_app():
    app = FastAPI(
        title = "ORKG-OSyRIS-API",
        root_path = os.getenv("ORKG_OSyRIS_API_PREFIX", ""),
        servers = [{
            "url": os.getenv("ORKG_OSyRIS_API_PREFIX", ""),
            "description": ""
        }],
        version = __version__
    )

    _configure_exception_handlers(app)
    _configure_cors_policy(app)
    _configure_app_assets(app)
    _configure_app_routes(app)

    _save_openapi_specification(app)

    return app

def _configure_app_assets(app):
    app.mount("/widget", StaticFiles(directory = "./app/static/public"), name = "static")

def _configure_app_routes(app):
    app.include_router(recommendation.router)
    app.include_router(submission.router)

def _configure_exception_handlers(app):
    async def validation_exception_handler(_, exc: RequestValidationError):
        return JSONResponse(
            status_code = status.HTTP_400_BAD_REQUEST,
            content = jsonable_encoder({ "detail": exc.errors(), "body": exc.body })
        )

    async def orkg_nlp_api_exception_handler(_, exc: OrkgOsyrisApiError):
        return JSONResponse(
            status_code = exc.status_code,
            content = jsonable_encoder({ "location": exc.class_name, "detail": exc.detail })
        )

    app.add_exception_handler(RequestValidationError, validation_exception_handler)
    app.add_exception_handler(OrkgOsyrisApiError, orkg_nlp_api_exception_handler)

def _configure_cors_policy(app):
    app.add_middleware(
        CORSMiddleware,
        allow_origins = [ "*" ],
        allow_methods = [ "*" ],
        allow_headers = [ "*" ],
        allow_credentials = False,
    )

def _save_openapi_specification(app):
    app_dir = os.path.dirname(os.path.realpath(__file__))
    ResponseWrapper.write_json(app.openapi(), os.path.join(app_dir, "..", "openapi.json"))