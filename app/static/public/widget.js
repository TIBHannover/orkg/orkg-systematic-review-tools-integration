"use strict";

// EVALUATE IN ANONYMOUS SCOPE (TO NOT WRITE TO window (GLOBAL) SCOPE)
(async () => {

    // DETERMINE DOCUMENT “RUNTIME” ENVIRONMENT, I.E. HOST AND QUERY
    const SERVICE_HOSTNAME = document.currentScript.src.match(/^http(s)?:\/\/[^/]+/)[0];
    const PARAMS = Object.fromEntries((document.currentScript.src
        .match(/\?[a-z]+(=[a-z]+)?(&[a-z]+(=[a-z]+)?)*$/i) || [ "" ])[0]
    .slice(1)
    .split(/&/g)
    .map(p => {
        return p.includes("=") ? p.split("=") : [ p, true ];
    }));
    
    // SOFTCODE CONFIGURATION OBJECT
    const _config = {
        customElementTagName: "orkg-export",
        truthReadCallbackAttributeName: "orkg-data-callback",
        endpointAuthPath: "/oauth/token",
        endpointComparisonPath: "/comparison",
        endpointInfoPath: "/widget/score-info.html",
        endpointRecommendPath: "/recommend",
        endpointRegisterPath: "/",
        endpointSubmitPath: "/submit",
        endpointWidgetMarkupPath: `/widget/widget.${PARAMS.lang || "en"}.html`,
        endpointWidgetStylesPath: "/widget/widget.css",
        eventNameError: "error",
        eventNameExport: "export",
        orkgHostname: `https://${PARAMS.dev ? "sandbox." : ""}orkg.org`
    };

    // HELPER FUNCTIONS
    /**
     * Backwards compatible HTTP(S) fetch.
     * Defaults to the configured ORKG host.
     */
    function compatibleFetch(path, method = "GET", body = null, headers = {}, hostname = SERVICE_HOSTNAME) {
        return new Promise((resolve, reject) => {
            const xhr = new XMLHttpRequest();
            xhr.onreadystatechange = () => {
                if(xhr.readyState !== 4) return;
                
                if(xhr.status !== 200) {
                    let err;
                    try {
                        err = JSON.parse(xhr.responseText).detail;
                    } catch {
                        err = xhr.responseText;
                    }

                    reject({
                        status: xhr.status,
                        message: err
                    });
                    
                    return;
                }

                resolve({
                    text: () => xhr.responseText,
                    json: () => JSON.parse(xhr.responseText)
                });
            };

            xhr.open(method, `${hostname}${path}${PARAMS.dev ? "?dev" : ""}`, true);

            for(let header in headers){
                xhr.setRequestHeader(header, headers[header]);
            }

            xhr.send(body);
        });
    }

    // RETRIEVE SCRIPT CONTENT DEPENDENCIES
    // in order not to block here, directly hardcode script contents into this file upon build (_ = `<code>`;)
    const markup = (await compatibleFetch(_config.endpointWidgetMarkupPath)).text();
    const styles = (await compatibleFetch(_config.endpointWidgetStylesPath)).text();
    
    // DECLARE ORKG EXPORT ELEMENT INHERITED FROM ORDINARY HTML ELEMENT CLaSS
    class ORKGExportElement extends HTMLElement {
        
        static parseCSVString(str, mappings = {
            authorNames: "paper:authors",
            paperDoi: "paper:doi",
            paperName: "paper:title",
            paperYear: "paper:year",
        }, excludeLabels = [], delim = ",", nestedDelim = ";") {
            if(delim === nestedDelim) throw new SyntaxError("Delimiter and nested delimiter must not be the same");
          
            const model = [];
            const lines = str.split(/\n/g);
            const criteriaLabels = lines.shift()
            .split(delim)
            .map(aspect => aspect.trim());

            lines
            .map(line => line.trim())
            .forEach(line => {
                const paper = {
                    authorNames: null,
                    paperName: null,
                    strongTruth: {}
                };

                line.split(delim)
                .map(value => value.trim())
                .forEach((value, i) => {
                    if(criteriaLabels[i] === mappings.authorNames) {
                        paper.authorNames = value
                        .split(nestedDelim)
                        .map(author => author.trim());
                        return;
                    }

                    for(let key in mappings) {
                        if(criteriaLabels[i] !== mappings[key]) continue;

                        paper[key] = value;
                        return;
                    }

                    if(!criteriaLabels[i] || excludeLabels.includes(criteriaLabels[i])) {
                        return;
                    }

                    paper.strongTruth[criteriaLabels[i]] = value || null;
                });

                model.push(paper);
            });
            
            return model;
        }
        
        constructor() {
            super();
        }

        // LIFECYCLE
        connectedCallback() {
            this.#shadowRoot = this.attachShadow({ mode: "closed" });

            const style = document.createElement("style");
            style.innerHTML = styles;
            this.#shadowRoot.appendChild(style);
            const template = document.createElement("template");
            template.innerHTML = markup;
            this.#shadowRoot.appendChild(template.content.cloneNode(true));

            const truthReadCallbackName = this.getAttribute(_config.truthReadCallbackAttributeName);
            if(!truthReadCallbackName) {
                throw new ReferenceError(`Missing data read callback attribute '${_config.truthReadCallbackAttributeName}'.`);
            }
            this.#truthReadCallback = window[truthReadCallbackName];
            if(!(this.#truthReadCallback instanceof Function)) {   // TODO: Defaults?
                throw new ReferenceError(`Passed data read callback is not a function '${truthReadCallbackName}'.`);
            }
            
            this.#shadowRoot.querySelector("#register-button")
            .addEventListener("click", () => window.open(`${_config.orkgHostname}${_config.endpointRegisterPath}`, "_blank"));
            this.#shadowRoot.querySelector("#export-button")
            .addEventListener("click", () => this.#toExport());
            this.#shadowRoot.querySelector("#submit-button--1")
            .addEventListener("click", () => this.#toAuth());
            this.#shadowRoot.querySelector("#submit-button--2")
            .addEventListener("click", () => this.#toSubmit());
            
            this.#shadowRoot.querySelector("#open-kg-button")
            .addEventListener("click", () => {
                window.open(this.#lastComparisonUrl, "_blank");
            });
            this.#shadowRoot.querySelector("#copy-url-button")
            .addEventListener("click", () => {
                navigator.clipboard.writeText(this.#lastComparisonUrl);
            });

            Array.from(this.#shadowRoot.querySelectorAll(".btn-close"))
            .forEach(button => {
                button.addEventListener("click", () => this.#closeModal());
            });
            Array.from(this.#shadowRoot.querySelectorAll(".btn-back"))
            .forEach(button => {
                button.addEventListener("click", () => this.#navigateBackModalWrapper());
            });

            this.#shadowRoot.querySelector("#scoring-info-button")
            .addEventListener("click", () => {
                window.open(`${SERVICE_HOSTNAME}${_config.endpointInfoPath}`, "_blank");
            });
        }
        
        #shadowRoot;
        #truthReadCallback;
        #truthData;
        #mappingData;
        #overflowBackupStyle;
        #wrapperHistory;
        #lastComparisonUrl;
        
        #readData() {
            try {
                this.#truthData = this.#truthReadCallback();
            } catch(err) {
                throw new EvalError(`Error invoking data read callback:\n${err}`);
            }

            const validateField = (index, obj, keys, validator = (() => true), typeHint = "any") => {
                keys = [ keys ].flat();
                for(let key of keys) {
                    try {
                        obj = obj[key];
                    } catch {
                        obj = null;

                        break;
                    }
                }

                if(obj && validator(obj)) return;
                
                throw new SyntaxError(`[Paper at index ${index}] The systematic review data is missing key ${keys.map(k => `"${k}"`).join("")}: ${typeHint}`);
            };
            
            for(let i = 0; i < this.#truthData.length; i++) {
                validateField(i, this.#truthData[i], "paperName", o => {
                    return (typeof(o) === "string") || (o instanceof String);
                }, "string");

                this.#truthData[i].authorNames = [ this.#truthData[i].authorNames ].flat();
                validateField(i, this.#truthData[i], "authorNames", o => {
                    return (typeof(o[0]) === "string") || (o[0] instanceof String);
                }, "string|string[]");
                
                validateField(i, this.#truthData[i], "strongTruth", o => {
                    return !!o;
                }, "{ [strong_truth_label: string]: string|number|boolean }");
            }
        }

        #openModal() {
            this.#overflowBackupStyle = document.body.style.overflow;
            document.body.style.overflow = "hidden";

            this.#shadowRoot.querySelector(".modal")
            .classList.add("active");

            setTimeout(() => {
                this.#shadowRoot.querySelector("#modal-auth form").classList.remove("error");
                Array.from(this.#shadowRoot.querySelectorAll("#modal-auth form input"))
                .forEach(input => {
                    input.value = "";
                });
            }, 400);
        }

        #closeModal() {
            this.#shadowRoot.querySelector(".modal")
            .classList.remove("active");

            this.#closeModalWrapper();

            document.body.style.overflow = this.#overflowBackupStyle;
        }

        #openModalWrapper(modalId) {
            this.#closeModalWrapper();
            
            this.#shadowRoot.querySelector(`#${modalId}`).classList.add("active");

            this.#shadowRoot.querySelector(".modal")
            .scrollTo(0, 0);

            this.#wrapperHistory.push(modalId);
        }

        #closeModalWrapper() {
            Array.from(this.#shadowRoot.querySelectorAll(".modal-wrapper"))
            .forEach(wrapperElement => {
                wrapperElement.classList.remove("active");
            });
        }

        #navigateBackModalWrapper() {
            this.#wrapperHistory.pop();
            this.#openModalWrapper(this.#wrapperHistory.pop());
        }

        #openModalError(err) {
            const modalErrorElement = this.#shadowRoot.querySelector("#modal-error");

            const errorMessage = `${
                ((err[PARAMS.lang] || err["en"]) || err.message) || ""
            }${
                err.detail ? `\n${err.detail}` : ""
            }`;

            modalErrorElement.querySelector("#error-message")
            .textContent = errorMessage;

            this.#openModalWrapper("modal-error");

            this.dispatchEvent(new CustomEvent(_config.eventNameError, {
                detail: errorMessage,
                composed: true
            }));
        }

        async #toExport() {
            this.#wrapperHistory = [];

            const rowTemplateElement = this.#shadowRoot.querySelector("#candidates-row-template");
            const mappingTableElement = this.#shadowRoot.querySelector("#modal-wrapper-recommendations tbody");

            this.#openModal();

            try {
                this.#readData();
            } catch(err) {
                this.#openModalError({
                    en: "The review data could not be parsed accordingly. Please refer to the systematic review service provider with regard of this issue:",
                    de: "Die Synthese-Daten konnten nicht angemessen ausgelesen werden. Bitte wenden sie sich an den Systematic Review Anbieter mit Bezug auf diesen Fehler:",
                    detail: err.message
                });

                return;
            }

            if(!this.#truthData.length) {
                this.#openModalError({
                    en: "The synthesis table does not yet contain any data.",
                    de: "Die Synthese-Tabelle enthält noch keine Daten."
                });
                
                return;
            }

            /* if(this.#truthData.map(paper => Object.values(paper.strongTruth)).flat().some(rec => [ undefined, null ].includes(rec))) {
                this.#openModalError({
                    en: "The synthesis table is incomplete, i.e. it contains empty cells. Please mark missing or empty data accordingly (e.g. 'N/A').",
                    de: "Die Synthese-Tabelle ist unvollständig, d.h. sie enthält leere Zellen. Bitte kennzeichnen die fehlende oder leere Daten entsprechend (z.B. 'N/A')."
                });

                return;
            } */

            mappingTableElement.textContent = "";
            this.#shadowRoot.querySelector("#modal-wrapper-recommendations-placeholder")
            .classList.remove("hidden");

            this.#shadowRoot.querySelector("#submit-button--1")
            .classList.add("disabled");
            
            this.#openModalWrapper("modal-edit");

            let recommendations;
            try {
                const recommendationTruth = this.#truthData
                .map(paper => paper.strongTruth)
                .reduce((acc, strongTruth) => {
                    for(let predicate in strongTruth) {
                        acc[predicate] = acc[predicate] || [];
                        acc[predicate].push(strongTruth[predicate]);
                    }
                    return acc;
                }, {});

                recommendations = (await compatibleFetch(_config.endpointRecommendPath, "POST", JSON.stringify({
                    payload: {
                        k: 3,
                        truth: recommendationTruth
                    }
                }), {
                    "Content-Type": "application/json"
                }))
                .json().payload;
            } catch(err) {
                console.error(err);

                this.#openModalError({
                    en: "The ORKG Systematic Review Integration Service cannot be reached at the moment.",
                    de: "Der ORKG Systematic Review Integration Service kann derzeit nicht erreicht werden."
                });
                
                return;
            }

            this.#shadowRoot.querySelector("#submit-button--1")
            .classList.remove("disabled");

            this.#mappingData = Object.fromEntries(Object.keys(recommendations)
                .map(predicateLabel => {
                    return [
                        predicateLabel, null
                    ];
                })
            );

            for(let strong_truth_label in recommendations) {
                const predicateCandidates = recommendations[strong_truth_label];

                const rowElement = rowTemplateElement.content.cloneNode(true);

                const tdElements = rowElement.querySelectorAll("td");
                const candidateSelectElements = Array.from(rowElement.querySelectorAll(".candidate-select"));
                const detailsDescriptionElement = rowElement.querySelector(".candidate-details a");
                
                let curScore = 0;
                
                let animateScoreTransitionTimeout;
                const selectCandidate = j => {
                    candidateSelectElements
                    .forEach(candidateSelectElement => {
                        candidateSelectElement.classList.remove("active");
                    });
                    const candidateSelectElement = candidateSelectElements[j + 1];
                    candidateSelectElement.classList.add("active");
                    
                    const originalLabel = candidateSelectElement.getAttribute("data-original");
                    const mappedIndex = candidateSelectElement.getAttribute("data-id");
                    
                    this.#mappingData[originalLabel] = (mappedIndex > -1)
                    ? {
                        label: recommendations[originalLabel][mappedIndex].label,
                        resourceId: recommendations[originalLabel][mappedIndex].details.resourceId,
                        resourceType: recommendations[originalLabel][mappedIndex].details.resourceType
                    } : null;
                    
                    tdElements[2].querySelector(".modal-wrapper-score")
                    .classList[(j === -1) ? "add" : "remove"]("invisible");

                    if(j === -1) {
                        detailsDescriptionElement.classList.add("hidden");
                        return;
                    };
                    
                    detailsDescriptionElement.classList.remove("hidden");
                    detailsDescriptionElement.textContent = predicateCandidates[j].details.description;
                    detailsDescriptionElement.setAttribute("href", `${_config.orkgHostname}${predicateCandidates[j].details.link}`);

                    const scoreBarElement = tdElements[2].querySelector(".modal-wrapper-score-bar").children[0];
                    const scoreNumElement = tdElements[2].querySelector(".modal-wrapper-score-num");
                    
                    const animateScoreTransition = (curScore, targetScore, forceDisplay = false) => {
                        if(!forceDisplay && curScore === targetScore) return;

                        curScore = !forceDisplay
                        ? curScore + ((targetScore - curScore) / Math.abs(targetScore - curScore))
                        : curScore;
                        const unitScore = curScore / 100;
                        const color = `rgb(${[ ((1 - unitScore) * 125) + 75, (unitScore * 125) + 75, unitScore * 75 ].join(",")})`;

                        scoreBarElement.style.width = `${curScore}%`;
                        scoreBarElement.style.backgroundColor = color;

                        scoreNumElement.textContent = `${curScore}%`;
                        scoreNumElement.style.color = color;
                        
                        animateScoreTransitionTimeout = setTimeout(() => animateScoreTransition(curScore, targetScore),
                            (1 / Math.max(Math.pow(Math.log(Math.abs(targetScore - curScore)), 2), 1)) * 20);
                    };
                    
                    const score = predicateCandidates[j].score;
                    clearTimeout(animateScoreTransitionTimeout);
                    animateScoreTransition(Math.round(curScore * 100), Math.round(score * 100), true);
                    curScore = score;
                };
                
                candidateSelectElements
                .forEach((candidateSelectElement, j) => {
                    j -= 1;

                    if(j >= predicateCandidates.length) {
                        candidateSelectElement.parentNode.removeChild(candidateSelectElement);
                        
                        return;
                    }

                    const predicateLabel = (j === -1) ? strong_truth_label : predicateCandidates[j].label;

                    candidateSelectElement.textContent = predicateLabel;

                    candidateSelectElement.setAttribute("data-original", strong_truth_label);
                    candidateSelectElement.setAttribute("data-id", j);

                    candidateSelectElement.addEventListener("click", () => selectCandidate(j));
                });
                
                if(!predicateCandidates.length) {
                    tdElements[0].parentNode.classList.add("disabled");
                    tdElements[1].textContent = "N/A";
                }
                
                selectCandidate(predicateCandidates.length ? 0 : -1); // TODO: Min

                mappingTableElement.appendChild(rowElement);
            }
            
            this.#shadowRoot.querySelector("#modal-wrapper-recommendations-placeholder")
            .classList.add("hidden");
        }

        #toAuth() {
            this.#shadowRoot.querySelector("#modal-auth").classList.remove("blocked");

            this.#openModalWrapper("modal-auth");
        }

        async #toSubmit() {
            let accessToken;
            try {
                const mail = this.#shadowRoot.querySelector("input[name=auth-mail]").value;
                const password = this.#shadowRoot.querySelector("input[name=auth-pass]").value;
                accessToken = (await compatibleFetch(_config.endpointAuthPath, "POST", `grant_type=password&username=${encodeURI(mail)}&password=${encodeURI(password)}`, {
                    "Content-Type": "application/x-www-form-urlencoded",
                    "Authorization": `Basic ${btoa("orkg-client:secret")}`
                }, _config.orkgHostname))
                .json().access_token;
            } catch(err) {
                this.#shadowRoot.querySelector("#modal-auth form").classList.add("error");
                this.#shadowRoot.querySelector("#modal-auth form input").focus();

                return;
            }

            this.#shadowRoot.querySelector("#modal-auth").classList.add("blocked");

            let contributionIds;
            try {
                contributionIds = (await compatibleFetch(_config.endpointSubmitPath, "POST", JSON.stringify({
                    payload: {
                        data: this.#truthData,
                        mappings: Object.fromEntries(Object.keys(this.#mappingData)
                            .filter(predicateLabel => this.#mappingData[predicateLabel])
                            .map(predicateLabel => {
                                return [
                                    predicateLabel, this.#mappingData[predicateLabel]
                                ];
                            })
                        )
                    }
                }), {
                    "Authorization": `Bearer ${accessToken}`,
                    "Content-Type": "application/json"
                }))
                .json().payload;
            } catch(err) {
                console.error(err);

                this.#openModalError({
                    en: "Your data cannot be published to ORKG at the moment. ORKG responded:",
                    de: "Ihre Daten können derzeit nicht im ORKG veröffentlicht werden. ORKG meldete:",
                    detail: err.message || "Unknown error"
                });

                return;
            }

            this.#lastComparisonUrl = `${_config.orkgHostname}${_config.endpointComparisonPath}?contributions=${contributionIds.join(",")}`;
            
            this.dispatchEvent(new CustomEvent(_config.eventNameExport, {
                detail: this.#lastComparisonUrl,
                composed: true
            }));

            this.#toComplete();
        }

        #toComplete() {
            this.#openModalWrapper("modal-complete");
        }
        
    }
    
    // BIND PREVIOUSLY DECLARED ORKG EXPORT ELEMENT TO WINDOW ELEMENT SET
    window.customElements.define(_config.customElementTagName, ORKGExportElement);

    window.ORKGExportElement = ORKGExportElement;

})();