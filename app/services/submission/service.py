import re
import uuid

from app.common.request import get, post, sparql
from app.models.submission import SubmissionRequestData
from app.services.recommendation.scoped_dbscan_scoring.scoped_dbscan_scoring import ScopedDBSCANScoring


def submit(data: SubmissionRequestData, is_dev_mode: bool = False, authorization: str = "") -> list[str]:
    contribution_resource_ids = []

    for paper in data["data"]:
        new_predicates = {}
        new_literals = {}
        statements = {}

        for strong_truth_label in paper["strongTruth"]:
            predicate_id: str
            try:
                predicate_id = data["mappings"][strong_truth_label]["resourceId"]
            except:
                predicate_id = "#_p_{}".format(strong_truth_label)
                new_predicates[predicate_id] = {
                    "label": strong_truth_label
                }
            
            if paper["strongTruth"][strong_truth_label] == None:
                continue

            literal_id = "#_l_{}".format(strong_truth_label)
            new_literals[literal_id] = {
                "label": paper["strongTruth"][strong_truth_label],
                "data_type": "xsd:{}".format(data["mappings"][strong_truth_label]["resourceType"] if (
                    strong_truth_label in data["mappings"]
                ) else ScopedDBSCANScoring.infer_dominant_type([ p["strongTruth"][strong_truth_label] for p in data["data"] ]))
            }

            statements[predicate_id] = [
                {
                    "id": literal_id,
                    "statements": None
                }
            ]
        
        paper_resource_id: str
        if "paperDoi" in paper:
            try:
                paper_resource_id = sparql("""
                    PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
                    PREFIX orkgp: <http://orkg.org/orkg/predicate/>

                    SELECT ?paper WHERE {
                    ?paper orkgp:P26 """ + paper["paperDoi"] + """^^xsd:string .

                    LIMIT 1
                """, is_dev_mode)[0]["paper"]
            except:
                paper_resource_id = None
        
        if "paperDoi" in paper and paper_resource_id != None:   # UPDATE
            contribution_location = post({
                "literals": new_literals,
                "predicates": new_predicates,
                "contribution": [
                    {
                        "label": "Contribution 1",
                        "classes": [],
                        "statements": statements
                    }
                ]
            }, "/api/contributions", None, {
                "Content-Type": "application/vnd.orkg.contribution.v2+json;charset=UTF-8",
                "Accept": "application/vnd.orkg.contribution.v2+json",
                "Authorization": authorization
            }, is_dev_mode)["headers"]["Location"]

            contribution_resource_id = re.search(r"\/[^/]+$", contribution_location)[0][1:]

            post({
                "Content-Type": "application/json;charset=utf-8",
                "Accept": "application/json"
            }, "/api/statements/", None, {
                "subject_id": paper_resource_id,
                "predicate_id": "P31",
                "object_id": contribution_resource_id
            }, is_dev_mode)
            paper_resource_id,contribution_resource_id
        else:   # CREATE
            paper_id = {
                "doi": paper["paperDoi"]
            } if "paperDoi" in paper else {
                "uuid": str(uuid.uuid4())
            }
            
            paper_location = post({
                "title": paper["paperName"],
                "authors": list(map(lambda name: {
                    "name": name
                }, paper["authorNames"])),
                "research_fields" : [],
                "identifiers" : paper_id,
                "publication_info" : {
                    "published_year" : paper["paperYear"] if "paperYear" in paper else None,
                },
                "observatories" : [],
                "organizations" : [],
                "contents": {
                    "literals": new_literals,
                    "predicates": new_predicates,
                    "contributions": [
                        {
                            "label": "Contribution 1",
                            "classes": [],
                            "statements": statements
                        }
                    ]
                },
                "extraction_method": "MANUAL"
            }, "/api/papers", None, {
                "Content-Type": "application/vnd.orkg.paper.v2+json;charset=UTF-8",
                "Accept": "application/vnd.orkg.paper.v2+json",
                "Authorization": authorization
            }, is_dev_mode)["headers"]["Location"]

            paper_resource_id = re.search(r"\/[^/]+$", paper_location)[0][1:]
        
        contribution_resource_id = get("/api/papers/{}".format(paper_resource_id), None, {
            "Content-Type": "application/vnd.orkg.paper.v2+json;charset=UTF-8",
            "Accept": "application/vnd.orkg.paper.v2+json"
        }, is_dev_mode)["data"]["contributions"][0]["id"]
        
        contribution_resource_ids.append(contribution_resource_id)
    
    return contribution_resource_ids