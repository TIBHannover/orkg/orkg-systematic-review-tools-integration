from app.models.recommendation import RecommendationRequestData, RecommendationResponseData
from app.services.recommendation import scoped_dbscan_scoring


sds = scoped_dbscan_scoring.ScopedDBSCANScoring(score_threshold=0.5)


def recommend(data: RecommendationRequestData) -> RecommendationResponseData:
    predicate_cadidates: RecommendationResponseData = {}
    
    for strong_truth_label in data["truth"]:
        sds_f = sds.fit({
            "predicate_label": strong_truth_label,
            "object_literals": data["truth"][strong_truth_label]
        })

        predicate_cadidates[strong_truth_label] = list(map(lambda t: {
            "label": t["predicate_label"],
            "score": t["score"],
            "details": {
                "resourceId": t["predicate_id"],
                "resourceType": t["predicate_range"],
                "description": "Details in ORKG",   # TODO: SHOW DESCRIPTIONS IF AVAILABLE
                "link": "/property/{}".format(t["predicate_id"])
            }
        }, sds_f))
    
    return predicate_cadidates