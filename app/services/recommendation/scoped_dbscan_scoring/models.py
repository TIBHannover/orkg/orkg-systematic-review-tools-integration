from typing import TypedDict
from typing_extensions import NotRequired


class Truth(TypedDict):    
    predicate_label: str
    object_literals: list[any]
    
    predicate_id: NotRequired[str]