import sys
import os
import json
import threading
import random
import datetime
import time

import app.services.recommendation.scoped_dbscan_scoring.embeddings as embeddings
from app.services.recommendation.scoped_dbscan_scoring.models import Truth
from app.common.request import sparql

import threading

def set_interval(func, sec):
    def func_wrapper():
        set_interval(func, sec)
        func()
    t = threading.Timer(sec, func_wrapper)
    t.start()
    return t

USE_DUMP = "usedump" in sys.argv[1:]
USE_DUMP = True                         # TODO: DISABLE DUMP ONLY IN PRODUCTION


class WeakTruthIndex():
    def __init__(self, limit = 100000):
        self.__limit = limit
        self.__kg_model: dict[str, Truth] = {}

        if USE_DUMP:
            self.__construct_dump()
            return
        
        def set_interval(func, sec):
            def construct_wrapper():
                set_interval(func, sec)
                self.__construct()
            t = threading.Timer(sec, construct_wrapper)
            t.start()
            return t

        thread = threading.Thread(target=self.__construct)
        thread.start()
    
    def __construct_dump(self):
        with open(os.path.dirname(__file__) + "/__dump.json") as json_file_in:
            kg_dump = json.load(json_file_in)
            
            def load_dump(datatype: str):
                self.__log_temp("Constructing index from partial ORKG dump (Oct 2023) [type: {}]".format(datatype))

                self.__kg_model[datatype] = []
                for label in kg_dump[datatype]:
                    if len(self.__kg_model[datatype]) >= 500:
                        break

                    instance = kg_dump[datatype][label]

                    literals = random.sample(instance["literals"], min(len(instance["literals"]), 50))
                    
                    embedded_literals = []
                    match datatype:
                        case "decimal":
                            embedded_literals = embeddings.embed_numbers(literals)
                        case _:
                            embedded_literals = embeddings.embed_strings(literals)
                    
                    self.__kg_model[datatype].append({
                        "predicate_label": label,
                        "predicate_id": instance["predicate"],
                        "object_literals": embedded_literals
                    })
            
            load_dump("decimal")
            load_dump("string")

            self.__log_temp("DONE constructing index from ORKG dump")
    
    def __log_temp(self, message: str):
        print("\x1b[1A\x1b[K\x1b[2m\x1b[5m\x1b[34m[{}] {}\x1b[0m".format(
            datetime.datetime.now().strftime("%Y-%M-%D %H:%M:%S"), message
        ))
    
    def __construct(self):
        def tstamp():
            return int(time.time_ns() / 1000000)
        def query_page(datatype: str, page: int = 0, page_size: int = 1000):
            return sparql("""
                PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
                PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>

                SELECT DISTINCT (?predicate AS ?predicateId) (?label as ?predicateLabel)
                    (GROUP_CONCAT(?literal; separator="; ") AS ?literalsList)
                WHERE {
                    ?subject ?predicate ?object .
                    FILTER(isLiteral(?object))
                    FILTER(datatype(?object) = xsd:""" + datatype + """)
                    ?predicate rdfs:label ?label .
                    BIND(str(?object) AS ?literal)
                }
                GROUP BY ?predicate ?label ?object
                LIMIT """ + page_size + """
                OFFSET """ + str(page * page_size))
        def query_type(datatype: str):
            self.__kg_model[datatype] = []

            page_size = 1000
            page: int = 0
            while True:
                self.__log_temp("Constructing index from live ORKG [type: {}, page: {}]".format(datatype, page))
                
                page_records = query_page("string", page, page_size)

                for page_record in page_records:
                    literals = page_record["literalsList"].split(";")
                    embedded_literals = []
                    match datatype:
                        case "decimal" | "integer" | "float":
                            embedded_literals = embeddings.embed_numbers(literals)
                        case "date":
                            embedded_literals = embeddings.embed_dates(literals)
                        case _:
                            embedded_literals = embeddings.embed_strings(literals)
                    
                    self.__kg_model[datatype].append({
                        "predicate_label": page_record["predicateLabel"],
                        "predicate_id": page_record["predicateId"],
                        "object_literals": embedded_literals
                    })

                    if len(self.__kg_model[datatype]) >= self.__limit: return
                
                if page_records < page_size: return

                page += 1

        start_time = tstamp()

        query_type("decimal")
        query_type("integer")
        query_type("float")
        query_type("date")
        query_type("string")

        self.__log_temp("DONE constructing index from live ORKG ({}s)".format(round(tstamp() - start_time)))

    
    def has(self, datatype: str) -> bool:
        return datatype in self.__kg_model
    
    def get(self, datatype: str) -> Truth:
        return self.__kg_model[datatype]
    
    def get_all(self) -> Truth:
        return self.__kg_model["decimal"] + self.__kg_model["string"]