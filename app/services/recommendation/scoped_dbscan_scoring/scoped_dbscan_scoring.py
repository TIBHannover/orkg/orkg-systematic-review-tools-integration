import statistics
import re
from typing import TypeVar, TypedDict

import app.services.recommendation.scoped_dbscan_scoring.embeddings as embeddings
import app.services.recommendation.scoped_dbscan_scoring.string_similarity as string_similarity
from app.services.recommendation.scoped_dbscan_scoring.models import Truth
from app.services.recommendation.scoped_dbscan_scoring.weak_index import WeakTruthIndex


class ScoreCandidate(TypedDict):
    predicate_id: str
    predicate_label: str
    score: float

T = TypeVar("T")


class ScopedDBSCANScoring:

    @staticmethod
    def infer_dominant_type(truth_literals: list[str]) -> str:
        def map_types(literal):
            if re.compile(r"^[0-9]+$").search(literal):
                return "integer"
            elif re.compile(r"^[0-9]+\.[0-9]+$").search(literal):
                return "decimal"
            elif re.compile(r"^[0-9]+\.[0-9]+[eE]\-?[0-9]+$").search(literal):
                return "float"
            elif re.compile(r"^[0-9]{1,2}([/.-])[0-9]{1,2}\1[0-9]{1,4}( *[0-9]{1,2}(:[0-9]{1,2}(:[0-9]{1,2})?)?)?$").search(literal):
                return "date"
            return "string"
        
        truth_literals = map(map_types, truth_literals.copy())
        try:
            return statistics.mode(truth_literals)
        except:
            return "string"
    
    def __init__(self, weak_truth_index: WeakTruthIndex = WeakTruthIndex(limit=100), k:int = 3, weight_label: float = 0.5, score_threshold: float = 0.25):
        self.__weak_truth_index = weak_truth_index
        self.__k = k
        self.__weight_label = weight_label
        self.__score_threshold = score_threshold

        self.__DBSCOPE_TOLERANCE = 0.25
    
    def __scoped_dbscan(self, literals_strong: list[T], literals_weak: list[T]):
        epsilon = statistics.stdev(literals_strong) * (1 + self.__DBSCOPE_TOLERANCE)
        fringe = [ min(map(lambda l: (l, abs(l - statistics.mean(literals_strong))), literals_weak), key = lambda t: t[1])[0] ]
        cluster = []
        
        while len(fringe) > 0:
            core = fringe.pop()
            if not core in literals_weak: continue
            literals_weak.remove(core)
            cluster.append(core)

            neighbours = []
            for o in literals_weak:
                if self.__geometric_distance(o, core) < epsilon:
                    neighbours.append(o)
            
            if len(neighbours) >= 2:
                fringe = fringe + neighbours
            elif len(cluster) == 0:
                return self.__scoped_dbscan(literals_strong, literals_weak)
        
        return cluster
    
    def __cluster_affiliation(self, cluster_strong: int|float, cluster_weak: int|float):
        def calc_cluster(cluster):
            return {
                "data": cluster,
                "centroid": statistics.mean(cluster) + 1,
                "radius": ((max(cluster) - min(cluster)) / 2) + 1
            }
        cluster_weak = calc_cluster(cluster_weak)
        cluster_strong = calc_cluster(cluster_strong)

        def unit_constrain_value(x):
            return max(0, min(1, x))
        
        cluster_s = cluster_strong if (cluster_strong["radius"] < cluster_weak["radius"]) else cluster_weak
        cluster_l = cluster_strong if (cluster_strong["radius"] >= cluster_weak["radius"]) else cluster_weak
        
        unit_delta = 1 - unit_constrain_value(self.__geometric_distance((cluster_s["centroid"] + cluster_s["radius"]), (cluster_l["centroid"] + cluster_l["radius"])) / (2 * cluster_s["radius"]))**2
        
        return unit_delta   # NO THETA IN 1D
    
    def __slice_score_list(self, score_list: list[ScoreCandidate], candidate: ScoreCandidate) -> list[ScoreCandidate]:
        if candidate["score"] < self.__score_threshold: return score_list
        
        score_list.append(candidate)
        if len(score_list) > self.__k:
            score_list.remove(min(score_list, key = lambda s: s["score"]))
        
        return sorted(score_list, reverse = True, key = lambda s: s["score"])

    def __geometric_distance(self, p1: int|float, p2: int|float) -> int|float:
        return abs(p1 - p2)
    
    def fit(self, truth_strong: Truth) -> list[ScoreCandidate]:
        truth_strong["object_literals"] = list(filter(lambda l: l != None, truth_strong["object_literals"]))

        dominant_type = ScopedDBSCANScoring.infer_dominant_type(truth_strong["object_literals"])
        
        all_truth_weak = self.__weak_truth_index.get(dominant_type) if self.__weak_truth_index.has(dominant_type) else []

        score_list: list[ScoreCandidate] = []
        for truth_weak in all_truth_weak:
            score = 0.0
            score_label = string_similarity.levenshtein(
                truth_strong["predicate_label"] if truth_strong["predicate_label"] else "",
                truth_weak["predicate_label"] if truth_weak["predicate_label"] else ""
            )

            literals_strong: list = truth_strong["object_literals"].copy()
            match dominant_type:
                case "decimal" | "integer" | "float":
                    literals_strong = embeddings.embed_numbers(literals_strong)
                case "date":
                    literals_strong = embeddings.embed_dates(literals_strong)
                case _:
                    literals_strong = embeddings.embed_strings(literals_strong)
            
            if (
                truth_strong["predicate_label"] == truth_weak["predicate_label"]
            ) or (
                len(literals_strong) < 2
            ) or (
                dominant_type not in [ "decimal", "integer", "float", "date", "string" ]
            ):
                score = score_label
            else:
                literals_weak: list = truth_weak["object_literals"].copy()
                
                objects_reference_cluster = self.__scoped_dbscan(literals_strong, literals_weak)
                
                score_literal = self.__cluster_affiliation(objects_reference_cluster, literals_strong)

                score = (self.__weight_label * score_label) + ((1 - self.__weight_label) * score_literal)
            
            score_list = self.__slice_score_list(score_list, {
                "predicate_id": truth_weak["predicate_id"],
                "predicate_label": truth_weak["predicate_label"],
                "predicate_range": dominant_type,
                "score": score
            })
        
        return score_list