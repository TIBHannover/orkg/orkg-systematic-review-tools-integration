def levenshtein(str1: str, str2: str):
    rows = len(str1) + 1
    cols = len(str2) + 1
    dist = [ [ 0 for _ in range(cols) ] for _ in range(rows) ]

    if rows == 1 and cols == 1:
        return 1.0
    
    if rows == 1 or cols == 1:
        return 0.0

    for i in range(1, rows):
        dist[i][0] = i
    
    for i in range(1, cols):
        dist[0][i] = i
        
    for col in range(1, cols):
        for row in range(1, rows):
            cost = 0 if(str1[row - 1] == str2[col - 1]) else 1

            dist[row][col] = min(dist[row - 1][col] + 1,
                                dist[row][col - 1] + 1,
                                dist[row - 1][col - 1] + cost)

    return 1.0 - (dist[row][col] / max(rows - 1, cols - 1))