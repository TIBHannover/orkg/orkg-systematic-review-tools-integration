from sentence_transformers import SentenceTransformer, util


MODEL = SentenceTransformer("all-mpnet-base-v2")


def embed_numbers(nums: list[int|float]):  # IDENTITY
    return list(map(lambda n: float(n), filter(lambda n: isinstance(n, int) or isinstance(n, float), nums)))


def embed_dates(dates: list[str|int|float]):
    import datetime

    dates = list(filter(lambda s: isinstance(s, str), dates))
    
    embeddings = []
    for date in dates:
        # TODO: HANDLE MORE DATE PATTERNS
        date_obj = datetime.datetime.strptime(date, "%Y-%m-%d %H:%M:%S")
        unix_timestamp = int(date_obj.timestamp())
        
        embeddings.append(unix_timestamp)
    
    return embeddings


def embed_strings(strs: list[str]):
    strs = list(filter(lambda s: isinstance(s, str), strs))

    bare_embeddings = MODEL.encode(list(map(lambda s: str(s), strs)))

    dist_matrix_2d = []
    for i in range(len(strs)):
        row = []
        for j in range(len(strs)):
            row.append(1 - _unit_cosine_similarity(bare_embeddings[i], bare_embeddings[j]))
        dist_matrix_2d.append(row)
    
    if len(dist_matrix_2d) == 0:
        return []
    
    return list(_multi_dim_scaling(dist_matrix_2d))

def _unit_cosine_similarity(v1, v2):
    return float(util.cos_sim(v1, v2)[0][0])

def _multi_dim_scaling(dist_matrix_2d: list[list[float]]):
    import numpy as np
    
    dist_matrix_2d = np.array(dist_matrix_2d)

    distances_squared = np.square(dist_matrix_2d)

    n = dist_matrix_2d.shape[0]
    J = np.eye(n) - np.ones((n, n)) / n
    B = -0.5 * J @ distances_squared @ J
    
    eigenvalues, eigenvectors = np.linalg.eigh(B)

    sorted_indices = np.argsort(eigenvalues)[::-1]
    eigenvalues_sorted = eigenvalues[sorted_indices]
    eigenvectors_sorted = eigenvectors[:, sorted_indices]

    return np.sqrt(eigenvalues_sorted[0]) * eigenvectors_sorted[:, 0]