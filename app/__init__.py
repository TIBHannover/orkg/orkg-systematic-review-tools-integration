import logging
import os

import dotenv


dotenv.load_dotenv()

level = logging.getLevelName(os.environ.get("ORKG_OSyRIS_API_LOG_LEVEL", "DEBUG").upper())

logger = logging.getLogger(__name__)
logger.setLevel(level = level)

stdout = logging.StreamHandler()
stdout.setLevel(level = level)

formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s: %(message)s")
stdout.setFormatter(formatter)

logger.addHandler(stdout)