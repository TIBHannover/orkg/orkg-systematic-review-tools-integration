#!/bin/bash

echo "Make sure the service is running on localhost."


{
    open "$(dirname "$0")"/sandbox.en.html
} || {
    start "$(dirname "$0")"/sandbox.en.html
} || {
    xdg-open "$(dirname "$0")"/sandbox.en.html
}