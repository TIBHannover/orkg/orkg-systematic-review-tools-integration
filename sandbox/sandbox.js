const vDom = {};

document.addEventListener("DOMContentLoaded", async () => {
    
    vDom.rowTemplate = document.querySelector("template");
    vDom.table = document.querySelector("table").tBodies[0];
    
    const rows = (localStorage.getItem("synthesis") || "")
    .split("\n")
    .slice(1);
    rows.forEach(row => {
        addTableRow(row.split(",").slice(3));
    });
    addTableRow();

    const orkgExportElement = document.querySelector("orkg-export");
    orkgExportElement.addEventListener("export", e => {
        console.log("The ORKG export was successful:\n" + e.detail);
    });
    orkgExportElement.addEventListener("error", e => {
        console.log("The ORKG export caused an error:\n" + e.detail);
    });
});

window.addEventListener("beforeunload", () => {
    localStorage.setItem("synthesis", customTableReadCallback(true));
});

function addTableRow(record) {
    vDom.table.appendChild(vDom.rowTemplate.content.cloneNode(true));
    row = vDom.table.children[vDom.table.children.length - 1];
    row.querySelector(".study-id").textContent = vDom.table.children.length - 1;

    record
    && Array.from(row.querySelectorAll("td")).slice(1)
    .forEach((td, i) => {
        td.querySelector("input").value = record[i];
    });
    if(record) return;

    const addSuccessiveTableRow = () => {
        row.removeEventListener("input", addSuccessiveTableRow);

        addTableRow(null, true);
    };
    row.addEventListener("input", addSuccessiveTableRow);
}

function clearTable() {
    while(vDom.table.children.length > 1) {
        vDom.table.removeChild(vDom.table.lastChild);
    }

    addTableRow(null, true);
}

function customTableReadCallback(asCSV = false) {
    const tableData = [];

    Array.from(vDom.table.querySelectorAll("tr"))
    .forEach((row, i) => {
        const rowData = []
        .concat = (i === 0)
        ? [ "paper:authors", "paper:title" ]
        : [ (Math.random() < 0.5)
            ? `Example Author ${i}`
            : [ `Example Author ${i}.1`, `Example Author ${i}.2` ].join("; "),
                `Example Paper ${i} [rand.seed.: ${Math.round(Math.random() * 899999) + 100000}]`
            ];
        
        Array.from(row.querySelectorAll("th, td"))
        .forEach(cell => {
            rowData.push(
                (cell.textContent.trim() || (cell.querySelector("input, textarea").value || "").trim())
                || null
            );
        });
        
        (rowData.filter(literal => literal).length > 3)
        && tableData.push(rowData.join(", "));
    });

    const serialTableData = tableData.join("\n");
    return !asCSV
    ? ORKGExportElement.parseCSVString(serialTableData, {
        authorNames: "paper:authors",
        paperName: "paper:title"
    }, [ "# Study" ])
    : serialTableData;
}